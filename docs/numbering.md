# Definition Of Numbering

```
+--------------+-----------------+------------------+-------------+---------+
| 01           | 0000000000      | 0000000000000000 | 0000       | 0        |
+--------------+-----------------+------------------+------------+----------+
| Type of code | Type of product | Product Serial   | Anti cheat | Reserved |
+--------------+-----------------+------------------+------------+----------+
```
Type of code:
```
00 : barcode
01 : qrcode
```
Type of product:
```
The code of product
```
Product Serial:
```
The serial of a single product
```
Anti Cheat
```
A section to anti cheat
```
Reserved:
```
Reserved section
```
