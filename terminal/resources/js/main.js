/**
 * main.js - The main javascript of the site
 *
 * Copyright (C) 2018, SpartaEN <z884421380@gmail.com>
 *
 * This file is part of the bottle-recycling-cashier
 *
 * bottle-recycling-cashier is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * bottle-recycling-cashier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bottle-recycling-cashier; see the file COPYING. If not, see
 * <http://www.gnu.org/licenses/>.
 */
// SET UP client here
var sv_url = "";
var sv_cid = "";
var sv_key = "";
var cl_url = "";
var cl_sid = "";
var cl_sec = "";
var callback;
var sess = false;
// Handle a preload error with this var
var sf = false;
var avai_pages = ['welcome', 'status', 'result']
$(document).ready(function (e) {
    brc_newsession();
    // A message loop to get session status
    setInterval(function () {
        brc_sess_query();
        qrcode();
        if (sess == false) {
            // Session fails goes here
            if (sf == true) {
                alert('Session Failed!');
            }
            sf = true;
        }
        if (sess['status'] == 1) {
            // Some code to handle the awaiting page
        }
        if (sess['status'] == 0) {
            // Some code to handle the finish page
            switch_page('result');
            brc_newsession();
            setTimeout(function (e) {
                switch_page('welcome');
            }, 3000);
        }
        if (sess['status'] == 2) {
            switch_page('status');
            $('#sc-a').empty();
            $('#sa-a').empty();
            $('#sc-a')[0].append(sess['count']);
            $('#sa-a')[0].append(sess['amount']);
        }
    }, 500);
})
// Get a new session
function brc_newsession() {
    ajax('newsession', '', function (result) {
        if (result['status'] == 0) {
            cl_sid = result['data']['sid']
            cl_sec = result['data']['sec']
        } else {
            alert('ERROR')
        }
    });
}
// Query status
function brc_sess_query() {
    ajax('query', '', function (rtn) {
        if (rtn['status'] == 0) {
            sess = rtn['data'];
        } else {
            // Some code to handle session fail
            brc_newsession();
            // Not reasonable
            sess = false;
        }
    })
}
// Add bottles
function brc_sess_add(item) {
    ajax('add', item, function (data) {
        if (data['status'] == 0) {
            sess = data['data']
        } else {
            // Some code to handle failire
            sess = false;
        }
    });
}
// Finish payments
function brc_sess_finish() {
    ajax('finish', '', function (data) {
        if (data['status'] == 0) {
            data['status'] = data['data'];
        } else {
            // Some code to handle failire
            sess = false;
        }
    });
}
// Update QR code
function qrcode() {
    $('#qrcode').empty();
    new QRCode($('#qrcode')[0], cl_url + '?sessionid=' + cl_sid + '&secret=' + cl_sec);
}
function ajax(act, item = '', callback) {
    var req = { 'cid': sv_cid, 'key': sv_key, 'action': '', 'sid': cl_sid, 'item': item };
    switch (act) {
        case 'query':
            req['action'] = 'query';
            break;
        case 'newsession':
            req['action'] = 'newsession';
            break;
        case 'finish':
            req['action'] = 'finish';
            break;
        case 'add':
            req['action'] = 'add';
            break;
    }
    $.post(sv_url, req, function (result) {
        callback(JSON.parse(result));
    })
}
// switch page
function switch_page(page) {
    $.each(avai_pages, function (key, val) {
        var name = '#' + val;
        $(name).css('display', 'none');
    })
    $('#' + page).css('display', 'block');
}
