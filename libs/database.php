<?php
/**
 * libs/database.php - A model for database accessing
 *
 * Copyright (C) 2018, Haochen Zheng <z884421380@gmail.com>
 *
 * This file is part of the bottle-recycling-cashier
 *
 * bottle-recycling-cashier is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * bottle-recycling-cashier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bottle-recycling-cashier; see the file COPYING. If not, see
 * <http://www.gnu.org/licenses/>.
 */
/**
 * The basic module for database accessing
 * @package    core
 * @subpackage database
 * @author     Haochen Zheng
 * @copyright  2018 Haochen Zheng https://sparta-en.org/
 * @version    v0.0.1
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class database
{
    public $db_conn;
    public $count;
    public function __construct(
        $db_type,
        $db_addr,
        $db_user,
        $db_pass,
        $db_name,
        $db_port
    ) {
        switch ($db_type) {
        case 'mysqli':
            $this->db_conn = new mysqli(
                $db_addr,
                $db_user,
                $db_pass,
                $db_name,
                $db_port
            );
            if ($this->db_conn->connect_error) {
                die('ERROR ENSTABLISHING A CONNCETION TO DATABASE');
            }
        }
    }
    public function __destruct()
    {
        $this->db_conn->close();
    }
    public function query($query)
    {
        $result = $this->db_conn->query($query);
        if ($result === false) {
            $error = $this->db_conn->errno.': '.$this->db_conn->error;
            $rtn = array(
                'status' => false,
                'count'  => 0,
                'result' => '',
                'error'  => $error
            );
        } elseif ($result === true) {
            $this->count = $this->db_conn->affected_rows;
            $rtn = array(
                'status' => true,
                'count'  => $this->count,
                'result' => '',
                'error'  => ''
            );
            return $rtn;
        } else {
            $this->count = $result->num_rows;
            $data = array();
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            $result->close();
            $rtn = array(
                'status' => true,
                'count'  => $this->count,
                'result' => $data,
                'error'  => ""
            );
            return $rtn;
        }
    }
    public function escape($str)
    {
        return $this->db_conn->real_escape_string($str);
    }
}
$DB = new database(
    $CFG->db_type,
    $CFG->db_addr,
    $CFG->db_user,
    $CFG->db_pass,
    $CFG->db_name,
    $CFG->db_port
);
