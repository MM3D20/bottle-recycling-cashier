<?php
/**
 * libs/lib.php - The core library
 *
 * Copyright (C) 2018, Haochen Zheng <z884421380@gmail.com>
 *
 * This file is part of the bottle-recycling-cashier
 *
 * bottle-recycling-cashier is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * bottle-recycling-cashier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bottle-recycling-cashier; see the file COPYING. If not, see
 * <http://www.gnu.org/licenses/>.
 */
/**
 * The basic module for database accessing
 * @package    core
 * @subpackage main
 * @author     Haochen Zheng
 * @copyright  2018 Haochen Zheng https://sparta-en.org/
 * @version    v0.0.1
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__.'/../config.php');
require_once(__DIR__.'/database.php');
require_once(__DIR__.'/query.php');
header('Access-Control-Allow-Origin: *');
// Construct config
$CFG->t_s = $CFG->db_prefix.'session';
$CFG->t_b = $CFG->db_prefix.'bottles';
$CFG->t_a = $CFG->db_prefix.'authorized';
// Sanitize string to avoid SQL injecting
function sanitize_strings($str)
{
    global $DB;
    $str = strip_tags($str);
    $str = htmlentities($str);
    $str = stripslashes($str);
    return $DB->escape($str);
}
function gen_session($cid)
{
    do {
        $time = time();
        $salt = rand(0, 1000);
        $sid = md5($cid.$time.$salt);
    } while (query::get_session($sid) !== false);
    return $sid;
}
