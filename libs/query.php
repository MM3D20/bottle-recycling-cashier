<?php
/**
 * libs/query.php - The main script to query database
 *
 * Copyright (C) 2018, Haochen Zheng <z884421380@gmail.com>
 *
 * This file is part of the bottle-recycling-cashier
 *
 * bottle-recycling-cashier is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * bottle-recycling-cashier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bottle-recycling-cashier; see the file COPYING. If not, see
 * <http://www.gnu.org/licenses/>.
 */
/**
 * The basic module for database accessing
 * @package    database
 * @subpackage main
 * @author     Haochen Zheng
 * @copyright  2018 Haochen Zheng https://sparta-en.org/
 * @version    v0.0.1
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class query
{
    public static function get_session($sid)
    {
        global $DB;
        global $CFG;
        $t_s = $CFG->t_s;
        $sid = sanitize_strings($sid);
        $result = $DB->query("SELECT * FROM `$t_s` WHERE sessionid='$sid'");
        if ($result['status'] === false || $result['count'] == 0) {
            return false;
        } else {
            return $result;
        }
    }
    public static function verify_session($sid, $secret)
    {
        $sid =  sanitize_strings($sid);
        $secret = sanitize_strings($secret);
        $result = self::get_session($sid);
        if ($result['status'] == true) {
            if ($result['result'][0]['secret'] == $secret) {
                return true;
            }
        }
        return false;
    }
    public static function pri_verify_session($cid, $pass)
    {
        global $DB;
        global $CFG;
        $t_a = $CFG->t_a;
        $cid = sanitize_strings($cid);
        $pass = sanitize_strings($pass);
        $result = $DB->query("SELECT * FROM `$t_a` WHERE cid = '$cid'");
        if ($result['status'] == true && $result['count'] == 1) {
            if ($result['result'][0]['pass'] == $pass) {
                // TODO:Update timestamp and IP address
                return true;
            }
        }
        return false;
    }
    public static function pri_query($sid)
    {
        global $DB;
        global $CFG;
        $sid = sanitize_strings($sid);
        $t_s = $CFG->t_s;
        $result = $DB->query("SELECT * FROM `$t_s` WHERE sessionid = '$sid'");
        return $result;
    }
    public static function pri_update($cid, $sid, $item)
    {
        global $DB;
        global $CFG;
        $amt = 0;
        $sid = sanitize_strings($sid);
        $item = sanitize_strings($item);
        $t_s = $CFG->t_s;
        $t_b = $CFG->t_b;
        /** Bottle status
         * 0 => finished
         * 1 => holding
         * 2 => cheated
         */
        $result = $DB->query("SELECT * FROM `$t_b` WHERE item = '$item'");
        $sess = $DB->query("SELECT * FROM `$t_s` WHERE sessionid = '$sid'");
        if ($result['count'] == 1 && $sess['count'] == 1) {
            if ($result['result'][0]['status'] == 1 && $sess['result'][0]['status'] == 2) {
                $amt = $result['result'][0]['amount'];
                $DB->query("UPDATE `$t_b`SET status = '0',sessionid = '$sid',cid = '$cid' WHERE item = '$item'");
            } else {
                return false;
            }
        } else {
            return false;
        }
        if (!$DB->query("UPDATE `$t_s` SET amount = amount + $amt, count = count + 1 WHERE sessionid = '$sid'")) {
            return false;
        }
        if ($onerr == false) {
            return $DB->query("SELECT * FROM `$t_s` WHERE sessionid = '$sid'");
        }
        return false;
    }
    public static function pri_newsession($cid)
    {
        global $DB;
        global $CFG;
        $t_s = $CFG->t_s;
        $sid = gen_session($cid);
        // Random function requires improvement
        $sec = md5(time().rand(1, 100));
        if ($DB->query("INSERT INTO `$t_s` VALUES ('$sid','$sec',0,0,null,1)")) {
            /** Session status
             * 0 => finished
             * 1 => holding
             * 2 => engaged
             */
            $rtn = array(
            'status' => true,
            'sid' => $sid,
            'sec' => $sec
        );
        } else {
            $rtn['status'] = false;
        }
        return $rtn;
    }
    public static function pri_finish($sid)
    {
        global $DB;
        global $CFG;
        $sid = sanitize_strings($sid);
        $t_s = $CFG->t_s;
        $result = $DB->query("SELECT * FROM `$t_s` WHERE sessionid = '$sid'");
        if ($result['count'] == 1 && $result['result'][0]['status'] != 0) {
            /**
             * Some code to handle PAYMENT
             */
            if ($DB->query("UPDATE `$t_s` SET status = 0 WHERE sessionid = '$sid'")) {
                return true;
            }
        } else {
            return false;
        }
    }
    public static function start_session($sid)
    {
        global $DB;
        global $CFG;
        $sid = sanitize_strings($sid);
        $t_s = $CFG->t_s;
        if ($DB->query("UPDATE `$t_s` SET status = 2 WHERE sessionid = '$sid'") == true) {
            return true;
        } else {
            return false;
        }
    }
    public static function finish($sid){
        global $DB;
        global $CFG;
        $sid = sanitize_strings($sid);
        $t_s = $CFG->t_s;
        // Call payment API
        if ($DB->query("UPDATE `$t_s` SET status = 0 WHERE sessionid = '$sid'") == true) {
            return true;
        } else {
            return false;
        }
    }
}
