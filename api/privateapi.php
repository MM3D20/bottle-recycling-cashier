<?php
/**
 * api/index.php - The private api of BRC
 *
 * Copyright (C) 2018, Haochen Zheng <z884421380@gmail.com>
 *
 * This file is part of the bottle-recycling-cashier
 *
 * bottle-recycling-cashier is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * bottle-recycling-cashier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bottle-recycling-cashier; see the file COPYING. If not, see
 * <http://www.gnu.org/licenses/>.
 */
/**
 * The basic module for database accessing
 * @package    api
 * @subpackage private
 * @author     Haochen Zheng
 * @copyright  2018 Haochen Zheng https://sparta-en.org/
 * @version    v0.0.1
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__.'/../libs/lib.php');
$rtn = array(
    'status' => 1,
    'mess'   => 'Unaudhorized',
    'data'   => []
);
if (isset($_POST['cid']) && isset($_POST['key'])) {
    if (query::pri_verify_session($_POST['cid'], $_POST['key']) == true) {
        if (isset($_POST['action'])) {
            switch ($_POST['action']) {
                case 'query':
                    $result = query::pri_query($_POST['sid']);
                    if ($result['count'] != 0) {
                        $rtn['status'] = 0;
                        $rtn['mess'] = 'SUCCESS';
                        $rtn['data'] = $result['result'][0];
                        echo json_encode($rtn);
                        exit();
                    }
                    $rtn['mess'] = 'ERROR';
                    echo json_encode($rtn);
                    exit();
                break;
                case 'add':
                    $result = query::pri_update($_POST['cid'], $_POST['sid'], $_POST['item']);
                    if ($result == false) {
                        $rtn['mess'] = 'ERROR';
                        echo json_encode($rtn);
                        exit();
                    } elseif ($result == 1) {
                        $rtn['mess'] = 'UPDATE FAILED';
                        echo json_encode($rtn);
                        exit();
                    } else {
                        $rtn['status'] = 0;
                        $rtn['mess'] = 'SUCCESS';
                        $rtn['data'] = $result['result'][0];
                        echo json_encode($rtn);
                        exit();
                    }
                break;
                case 'finish':
                    $result = query::pri_finish($_POST['sid']);
                    if ($result == true) {
                        $rtn['status'] = 0;
                        $rtn['mess'] = 'SUCCESS';
                        echo json_encode($rtn);
                        exit();
                    }
                    $rtn['mess'] = 'ERROR';
                    echo json_encode($rtn);
                    exit();
                break;
                case 'newsession':
                    $result = query::pri_newsession($_POST['cid']);
                    if ($result['status'] === true) {
                        $rtn['status'] = 0;
                        $rtn['mess'] = 'SUCCESS';
                        $rtn['data'] = $result;
                        echo json_encode($rtn);
                        exit();
                    }
                    $rtn['mess'] = 'ERROR';
                    echo json_encode($rtn);
                    exit();
                break;
            }
        } else {
            $rtn['status'] = 2;
            $rtn['mess'] = 'INVALID ARGUMENT';
        }
    }
}
echo json_encode($rtn);
