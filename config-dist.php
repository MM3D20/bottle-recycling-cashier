<?php
$CFG = new stdClass;
/*
    Your database config goes here.
    We only support mysqli currently.
*/
$CFG->db_type = '';
$CFG->db_addr = '';
$CFG->db_user = '';
$CFG->db_pass = '';
$CFG->db_name = '';
$CFG->db_port = '';
/*
    The title of the site
*/
$CFG->title = '';
/*
    The datatable prefix of moodle-class-manager
*/
$CFG->db_prefix = '';
/*
    The datatable prefix of moodle default is "mdl_"
*/
$CFG->db_destprefix = '';
/*
    The base url of your app
*/
$CFG->baseurl = '';
