<?php
/**
 * index.php - The main site for user
 *
 * Copyright (C) 2018, Haochen Zheng <z884421380@gmail.com>
 *
 * This file is part of the bottle-recycling-cashier
 *
 * bottle-recycling-cashier is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * bottle-recycling-cashier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bottle-recycling-cashier; see the file COPYING. If not, see
 * <http://www.gnu.org/licenses/>.
 */
/**
 * The basic module for database accessing
 * @package    core
 * @subpackage main
 * @author     Haochen Zheng
 * @copyright  2018 Haochen Zheng https://sparta-en.org/
 * @version    v0.0.1
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__.'/libs/lib.php');
session_start();
if (isset($_GET['sessionid']) && isset($_GET['secret'])) {
    if (query::verify_session($_GET['sessionid'], $_GET['secret']) === true) {
        $_SESSION['status'] = true;
    } else {
        die('INVALID SESSION');
    }
} else {
    die('INVALID SESSION');
}
$_SESSION['data'] = query::get_session($_GET['sessionid'])['result'][0];
if ($_SESSION['data']['status'] == 0) {
    // Handle completed or broken session
    die('SESSION COMPLETED');
}
if (!query::start_session($_GET['sessionid'])) {
    die('SESSION BROKEN');
}
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'fin') {
        query::finish($_GET['sessionid']);
        echo <<<_EOF
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BRC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        html,
        body {
            font-family: Arial, Helvetica, sans-serif;
            background-color: #eee;
        }

        .main {
            position: absolute;
            top: 50%;
            width: 100%;
            height: 80px;
            margin: -40px 0 0 0;
        }

        .text {
            color: black;
            font-weight: bold;
            width: 100%;
            text-align: center;
        }

        .btn {
            align-self: center;
            border: 1px solid #fff;
            background-color: #00b80f;
            color: #fff;
            font-weight: bold;
            border-radius: 5px;
            text-align: center;
            padding: 15px;
        }
    </style>
</head>

<body>
    <div class="main">
        <div class="text">已完成支付</div>
    </div>
</body>

</html>
_EOF;
        exit();
    }
}
$link = "index.php?sessionid=".$_GET['sessionid']."&secret=".$_GET['secret']."&action=fin";
echo <<<_EOF
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BRC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        html,
        body {
            font-family: Arial, Helvetica, sans-serif;
            background-color: #eee;
        }

        .main {
            position: absolute;
            top: 50%;
            width: 100%;
            height: 80px;
            margin: -40px 0 0 0;
        }

        .text {
            color: black;
            font-weight: bold;
            width: 100%;
            text-align: center;
        }

        .btn {
            align-self: center;
            border: 1px solid #fff;
            background-color: #00b80f;
            color: #fff;
            font-weight: bold;
            border-radius: 5px;
            text-align: center;
            padding: 15px;
        }
    </style>
</head>

<body>
    <div class="main">
        <div class="btn" onclick="location.href = '$link';">结算</div>
    </div>
</body>

</html>
_EOF;
